/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import java.sql.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
 
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Jean-Philippe
 */
public class Application {
  public static void main(String[] args) {
         
         
        // Read
        System.out.println("******* READ *******");
        List employees = EmployeeOperation.list();
        System.out.println("Total Employees: " + employees.size());
         
         
        // Write
        System.out.println("******* WRITE *******");
        Employee empl = new Employee("Jack", "Bauer", new Date(System.currentTimeMillis()), "911");
        empl = EmployeeOperation.save(empl);
        empl = EmployeeOperation.read(empl.getId());
        System.out.printf("%d %s %s \n", empl.getId(), empl.getFirstname(), empl.getLastname());
         
         
         
        // Update
        System.out.println("******* UPDATE *******");
        Employee empl2 = EmployeeOperation.read(1l); // read employee with id 1
        System.out.println("Name Before Update:" + empl2.getFirstname());
        empl2.setFirstname("James");
        EmployeeOperation.update(empl2);  // save the updated employee details
         
        empl2 = EmployeeOperation.read(1l); // read again employee with id 1
        System.out.println("Name Aftere Update:" + empl2.getFirstname());
         
         
        // Delete
        System.out.println("******* DELETE *******");
        EmployeeOperation.delete(empl); 
        Employee empl3 = EmployeeOperation.read(empl.getId());
        System.out.println("Object:" + empl3);
    }
}
