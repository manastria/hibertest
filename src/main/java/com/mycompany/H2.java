/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import java.sql.Connection;
import java.sql.DriverManager;
import org.h2.tools.Server;

/**
 *
 * @author Jean-Philippe
 */
public class H2 {
    private static Server server = null;
    private static Server webServer = null;
    
    private static String message = null;

    public static void startServer() throws Exception {
        String webPort = "8083";
        
        // start a TCP server
        server = Server.createTcpServer(new String[]{"-tcp", "-tcpAllowOthers", "-tcpPort", "8082"}).start();
        // Server webServer = Server.createWebServer("-web,-webAllowOthers,true,-webPort,8082").start();
        webServer = Server.createWebServer(new String[]{"-web", "-webAllowOthers", "-webPort", "8083"}).start();
        // Server server = Server.createTcpServer("-tcp,-tcpAllowOthers,true,-tcpPort,9092").start();

        message = String.format("%s/ -- jdbc:h2:%s/mem:test", webServer.getURL(), server.getURL());
        
        // .. use in embedded mode ..
        // or use it from another process:
        System.out.println("Server started and connection is open.");
        System.out.println("URL: jdbc:h2:" + server.getURL() + "/mem:test");

        // now start the H2 Console here or in another process using
        // java org.h2.tools.Console -web -browser
    }
    
    public static void closeServer() throws Exception {
        System.out.println("Stopping server and closing the connection");
        server.stop();
        webServer.stop();
    }

    public static String getMessage() {
        return message;
    }

    
    
    public static void main(String... args) throws Exception {

        // Création de la BDD en mémoire
        Class.forName("org.h2.Driver");
        Connection conn = DriverManager.getConnection("jdbc:h2:mem:test");
        conn.createStatement().execute("create table test(id int)");

        // start a TCP server
        Server server = Server.createTcpServer(new String[]{"-tcp", "-tcpAllowOthers", "-tcpPort", "8082"}).start();
        // Server webServer = Server.createWebServer("-web,-webAllowOthers,true,-webPort,8082").start();
        Server webServer = Server.createWebServer(new String[]{"-web", "-webAllowOthers", "-webPort", "8082"}).start();
		// Server server = Server.createTcpServer("-tcp,-tcpAllowOthers,true,-tcpPort,9092").start();

        // .. use in embedded mode ..
        // or use it from another process:
        System.out.println("Server started and connection is open.");
        System.out.println("URL: jdbc:h2:" + server.getURL() + "/mem:test");

        // now start the H2 Console here or in another process using
        // java org.h2.tools.Console -web -browser
        System.out.println("Press [Enter] to stop.");
        System.in.read();

        System.out.println("Stopping server and closing the connection");
        server.stop();
        webServer.stop();
        conn.close();
    }
}
